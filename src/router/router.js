import { createBrowserRouter } from "react-router-dom";
import App from "../views/App";
import PageError from "../views/PageError";

export const routes = createBrowserRouter([
  {
    path: "/",
    errorElement: <PageError />,
    element: <App />,
    children: []
  }
]);
