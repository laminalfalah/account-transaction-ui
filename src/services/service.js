import axios from 'axios';

const api = process.env.REACT_APP_API_URL;

export default axios.create({
  baseURL: api,
  timeout: 30000,
  withCredentials: true,
  headers: {
    "Accept": 'application/json',
    "Accept-Language": 'en',
    "Content-Type": 'application/json',
  }
});
