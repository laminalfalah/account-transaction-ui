import { useRouteError } from "react-router-dom";

function PageError() {
  const error = useRouteError();

  return (
    <>
      <h1>Oops</h1>
      <p>
        <i>{error.statusText || error.message}</i>
      </p>
    </>
  );
}

export default PageError;
